<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const AMAZON_SECRET_KEY = "U+XnowSxMZzs9nHHSaIZK0QbLPu3LvE2SLgRbKbN";
    const AMAZON_ACCESS_KEY = "AKIAIOC64JCDYGDPPOIQ";

    public static function saveAppSettingsByName($userSettings, $setting_name, $newSettings, $userId)
    {
        $settings = array();
        if (!empty($userSettings))
            $settings = json_decode($userSettings, true);
        if (!isset($settings[$setting_name]) || empty($settings[$setting_name]))
            $settings[$setting_name] = array();
        $requestArray = $newSettings;
        if (isset($settings[$setting_name])) {
            if (isset($requestArray['settings_mode']))
                unset($requestArray['settings_mode']);
            if (isset($requestArray['marketplace'])) {
                $marketplace = $requestArray['marketplace'];
                unset($requestArray['marketplace']);
                $settings[$setting_name][$marketplace] = $requestArray[$marketplace];
            } else if ($setting_name == 'formula_settings') {
                $id = $newSettings['id'];
                $formula_type = $newSettings['formula_type'];
                unset($newSettings['id']);
                unset($newSettings['formula_type']);
                $settings[$setting_name][$formula_type][$id] = $newSettings;
            } else {
                $settings[$setting_name] = $requestArray;
            }
        }
        self::saveAppSettings($settings, $userId);
    }

    public static function saveAppSettings($settings, $userId)
    {
        $user = self::find($userId);
        if (isset($user->id) && !empty($user->id)) {
            $user->settings = json_encode($settings);
            $user->save();
        }
    }

    public static function getUserInfo($is_api)
    {
        if ($is_api == '1') {
            return auth('api')->user();
        } else {
            return Auth::user();
        }
    }

    public static function getSetting($settingsJson, $setting_key = "")
    {
        $settings = array();
        if (!empty($settingsJson) && is_array(json_decode($settingsJson, true))) {
            $settingsArray = json_decode($settingsJson, true);
            if (!empty($settingsArray) && !empty($setting_key)) {
                if (isset($settingsArray[$setting_key]))
                    return $settingsArray[$setting_key];
                else
                    return [];
            } else {
                return $settingsArray;
            }
        }
        return $settings;
    }

    public static function constructAmazonStoreArray($amazonSettings)
    {
        return [
            'Marketplace_Id' => (isset($amazonSettings['martketplace_id'])) ? $amazonSettings['martketplace_id'] : $amazonSettings['marketplace_id'],
            'Seller_Id' => $amazonSettings['merchent_id'],
            'Access_Key_ID' => self::AMAZON_ACCESS_KEY,
            'Secret_Access_Key' => self::AMAZON_SECRET_KEY,
            'MWSAuthToken' => $amazonSettings['auth_token']
        ];
    }
    
    public static function constructDefaultAmazonStoreArray()
    {
        return [
            'Marketplace_Id' => 'ATVPDKIKX0DER',
            'Seller_Id' => 'A3LVZCSSZ88CPA',
            'Access_Key_ID' => self::AMAZON_ACCESS_KEY,
            'Secret_Access_Key' => self::AMAZON_SECRET_KEY,
            'MWSAuthToken' => 'amzn.mws.94b8c0bd-24c2-9b7a-4cbf-8de99a6440b0'
        ];
    }

    public static function findUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public static function getExtensionUser($id)
    {
        $userModel = new User;
        $userModel->setConnection(env('DB_EXTENSION_CONNECTION_NAME'));
        return $userModel->find($id);
    }
}
