@extends('layouts.auth')
@section('content')
    <div class="card">
        <div class="row align-items-center text-center">
            <div class="col-md-12">
                <div class="card-body">
                    <img src="{{asset('images/logo-dark.png')}}" alt="" class="img-fluid mb-4">
                    <h4 class="mb-3 f-w-400">Sign In</h4>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group mb-3 fill">
                            <label class="floating-label" for="email">Email address</label>
                            <input type="email" name="email" value="{{ old('email') }}"
                                   class="form-control @error('email') is-invalid @enderror" required autofocus
                                   id="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Password</label>
                            <input name="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password"
                                   type="password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <div class="custom-control custom-checkbox text-left mb-4 mt-2">
                            <input type="checkbox" class="custom-control-input" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Remember me.</label>
                        </div>
                        <button type="submit" class="btn btn-block btn-dark has-ripple mb-4">LOGIN</button>
                        <p class="mb-2 text-muted">Forgot password? <a href="{{ route('password.request') }}"
                                                                       class="f-w-400">Reset</a>
                        </p>
                        <p class="mb-0 text-muted">Don’t have an account? <a href="{{route('register')}}"
                                                                             class="f-w-400">Signup</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
