window.Vue = require('vue').default;

import VueRouter from "vue-router";
import route from './route';
import DataTable from 'laravel-vue-datatable';
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import vSelect from "vue-select";

const router = new VueRouter(route)
Vue.use(VueRouter)
Vue.use(DataTable);
Vue.use(VueLoading);

var apiDomain = "https://wmart.skurepricer.com";
if (location.host.indexOf('127.0.0.1') !== -1) {
    apiDomain = "http://127.0.0.1:8000";
}

Vue.component('loader', require('./components/Layouts/Loader').default);
Vue.component('navbar', require('./components/Layouts/NavBar').default);
Vue.component('headbar', require('./components/Layouts/HeadBar').default);
Vue.component('settings_navbar', require('./components/Layouts/SettingsNavbar').default);
Vue.component('settings_marketplace_navbar', require('./components/Layouts/MarketplaceNavbar').default);
Vue.component("v-select", vSelect);

const app = new Vue({
    el: '#wmart-app',
    router,
    data: {
        loading: false,
        fullPage: true,
        requestLoader: false,
        loader: 'bars',
        apiUrl: apiDomain + '/api',
        loggedInUser: []
    },
    components: {
        loading: VueLoading
    },
    watch: {
        $route: {
            immediate: true,
            handler(to, from) {
                document.title = 'Wmart' + to.meta.title || 'Wmart';
            }
        },
    },
    methods: {
        inArray: function (needle, haystack) {
            var length = haystack.length;
            for (var i = 0; i < length; i++) {
                if (haystack[i] == needle) return true;
            }
            return false;
        },
        getUserInfo: async function () {
            var userResponse = await this.sendRequest('/user/get-user', false, 'GET');
            if (userResponse.id !== undefined) {
                this.loggedInUser = userResponse;
            }
        },
        sendRequest: async function (url, auth, method, data) {
            var headers;
            if (auth === true) {
                headers = {
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + app.loggedInUser.api_token
                    },
                };
                if (data !== undefined)
                    headers.body = JSON.stringify(data)

            }
            this.requestLoader = true;
            var request = await fetch(this.apiUrl + url, headers);
            this.requestLoader = false;
            var response = await request.json();
            return response;
        }
    },
    created() {
        this.getUserInfo()
    }
});
router.beforeEach((to, from, next) => {
    app.loading = true
    next()
})

router.afterEach(() => {
    // setTimeout(() => app.loading = false, 2500) // timeout for demo purposes
    app.loading = false
})
