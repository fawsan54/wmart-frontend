<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amazon extends Model
{
    use HasFactory;

    public static function getVariations($amazonProductInfo)
    {
        $variations = array();
        if (isset($amazonProductInfo[0]['Relationships']) && !empty($amazonProductInfo[0]['Relationships']) && is_array($amazonProductInfo[0]['Relationships'])) {
            foreach ($amazonProductInfo[0]['Relationships'] as $relationship) {
                if (isset($relationship['Identifiers']['MarketplaceASIN']['ASIN']) && !empty($relationship['Identifiers']['MarketplaceASIN']['ASIN'])) {
                    $asin = $relationship['Identifiers']['MarketplaceASIN']['ASIN'];
                    unset($relationship['Identifiers']);
                    $variation = array();
                    foreach ($relationship as $variation_name => $variation_value) {
                        array_push($variation, $variation_name . " : " . $variation_value);
                    }
                    if (!empty($variation)) {
                        $variations[$asin] = implode(' | ', $variation);
                    }
                }
            }
        }
        return $variations;
    }

    public static function getSupplierUrl($supplierItemId)
    {
        if (\App\Models\Item::is_valid_url($supplierItemId))
            return $supplierItemId;
        return 'https://www.amazon.com/dp/' . $supplierItemId;
    }
}
