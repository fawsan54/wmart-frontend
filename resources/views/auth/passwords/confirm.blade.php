@extends('layouts.auth')
@section('content')
    <div class="card">
        <div class="row align-items-center text-center">
            <div class="col-md-12">
                <div class="card-body">
                    <img src="{{asset('images/logo-dark.png')}}" alt="" class="img-fluid mb-4">
                    <h4 class="mb-3 f-w-400">Confirm Password</h4>
                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf
                        {{ __('Please confirm your password before continuing.') }}
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Password</label>
                            <input name="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password"
                                   type="password" required autofocus>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Confirm Password</label>
                            <input name="password_confirmation"
                                   class="form-control @error('password_confirmation') is-invalid @enderror"
                                   id="password_confirmation"
                                   type="password" required>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-block btn-dark has-ripple mb-4">
                            Confirm Password
                        </button>
                        @if (Route::has('password.request'))
                            <p class="mb-0 text-muted">Forgot Your Password? <a href="{{ route('password.request') }}"
                                                                                class="f-w-400">Reset</a>
                            </p>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
