import IndexComponent from './components/Dashboard/IndexComponent';
import AllItemsComponent from './components/Items/AllItemsComponent';
import AddItemComponent from './components/Items/AddItemComponent';
import SettingsComponent from './components/Settings/IndexComponent';
import ProfitFormulaComponent from './components/Settings/ProfitFormulaComponent';
import MarketplaceComponent from "./components/Settings/MarketplaceComponent";
import AmazonMarketplaceComponent from "./components/Settings/AmazonMarketplaceComponent";
import SellingPriceFormulaComponent from "./components/Settings/SellingPriceFormulaComponent";

export default {
    mode: 'history',
    props: {
        default: true,
    },
    routes: [
        {
            path: '/dashboard',
            component: IndexComponent,
            name: 'dashboard',
            meta: {title: ' | Dashboard'}
        },
        {
            path: '/items/all',
            component: AllItemsComponent,
            name: 'allitems',
            meta: {title: ' | All Items'}
        },
        {
            path: '/items/add',
            component: AddItemComponent,
            name: 'additem',
            meta: {title: ' | Add Item'}
        },
        {
            path: '/items/edit/:id',
            component: AddItemComponent,
            name: 'edititem',
            meta: {title: ' | Edit Item'}
        },
        {
            path: '/settings',
            component: SettingsComponent,
            name: 'settings',
            meta: {title: ' | Settings'}
        },
        {
            path: '/settings/formula/profit-formula',
            component: ProfitFormulaComponent,
            name: 'profit formula',
            meta: {title: ' | Profit Formula'}
        },
        {
            path: '/settings/formula/selling-price-formula',
            component: SellingPriceFormulaComponent,
            name: 'selling price formula',
            meta: {title: ' | Selling Price Formula'}
        },
        {
            path: '/settings/marketplace/walmart',
            component: MarketplaceComponent,
            name: 'walmart marketplace',
            meta: {title: ' | Walmart Marketplace'}
        },
        {
            path: '/settings/marketplace/amazon',
            component: AmazonMarketplaceComponent,
            name: 'amazon marketplace',
            meta: {title: ' | Amazon Marketplace'}
        }
    ]
}
