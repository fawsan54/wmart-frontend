<?php


namespace App\Helpers;


class Walmart
{
    public function __construct($store_settings)
    {
        $this->url = "https://marketplace.walmartapis.com";
        $this->store_settings = $store_settings;
        $this->defaultHeader = array(
            'Authorization: Basic ' . base64_encode($store_settings['client_id'] . ":" . $store_settings['client_secret']),
            'WM_SVC.NAME: Wmart',
            'Accept: application/json',
            'WM_QOS.CORRELATION_ID: ' . time(),
        );
    }

    public function getInventory($accessToken, $sku)
    {
        $endpoint = "/v3/inventory?sku=" . $sku;
        $header = array(
            'WM_SEC.ACCESS_TOKEN: ' . $accessToken,
            'Content-Type: application/json',
        );
        $response = $this->callApi($endpoint, $header);
        if (!empty($response) && is_array(json_decode($response, true))) {
            $inventoryArray = json_decode($response, true);
            if (isset($inventoryArray['sku']) && !empty($inventoryArray['sku']))
                return $inventoryArray;
        }
        return 0;
    }

    public function authenticate()
    {
        $endpoint = "/v3/token";
        $response = $this->callApi($endpoint, array(), 'POST', 'grant_type=client_credentials');
        if (!empty($response) && is_array(json_decode($response, true))) {
            $resObject = json_decode($response);
            if (isset($resObject->access_token) && !empty($resObject->access_token)) {
                return (string)$resObject->access_token;
            } else {
                return false;
            }
        }
        return false;
    }

    public function getItemDetails($sku, $accessToken)
    {
        $endpoint = "/v3/items/$sku";
        $header = array(
            'WM_SEC.ACCESS_TOKEN: ' . $accessToken,
            'Content-Type: application/json',
        );
        $response = $this->callApi($endpoint, $header);
        if (!empty($response) && is_array(json_decode($response, true))) {
            $itemArray = json_decode($response, true);
            if (isset($itemArray['ItemResponse'][0]) && !empty($itemArray['ItemResponse'][0]))
                return $itemArray['ItemResponse'][0];
        }
        return array();
    }

    public function searchApi($accessToken)
    {
        $endpoint = "/v3/items/catalog/search?query=" . urlencode('marvel toys');
        $header = array(
            'WM_SEC.ACCESS_TOKEN: ' . $accessToken,
            'Content-Type: application/json',
        );
        $response = $this->callApi($endpoint, $header);
        if (!empty($response) && is_array(json_decode($response, true))) {
            $itemArray = json_decode($response, true);
            if (isset($itemArray['ItemResponse'][0]) && !empty($itemArray['ItemResponse'][0]))
                return $itemArray['ItemResponse'][0];
        }
        return array();
    }

    public function getItemPriceReport($accessToken)
    {
        $endpoint = '/v3/reports/reportRequests?reportType=ITEM&reportVersion=v4';
        $header = array(
            'WM_SEC.ACCESS_TOKEN: ' . $accessToken,
            'Content-Type: application/json',
        );
        $response = $this->callApi($endpoint, $header, 'POST');
        print_r($response);
        die;
        if (!empty($response) && is_array(json_decode($response, true))) {
            $itemArray = json_decode($response, true);
            if (isset($itemArray['ItemResponse'][0]) && !empty($itemArray['ItemResponse'][0])) {
                $inventoryData = $this->getInventory($accessToken, $itemArray['ItemResponse'][0]['sku']);
                $itemDetails[$itemArray['ItemResponse'][0]['sku']] = $itemArray['ItemResponse'][0];
                $itemDetails[$itemArray['ItemResponse'][0]['sku']]['inventory'] = $inventoryData;
                return $itemDetails;
            }
        }
        return array();
    }

    public function downloadInventory($accessToken, $sku)
    {
        $endpoint = '/v3/items/' . $sku;
        $header = array(
            'WM_SEC.ACCESS_TOKEN: ' . $accessToken,
            'Content-Type: application/json',
        );
        $response = $this->callApi($endpoint, $header);

        if (!empty($response) && is_array(json_decode($response, true))) {
            $itemArray = json_decode($response, true);
            if (isset($itemArray['ItemResponse'][0]) && !empty($itemArray['ItemResponse'][0])) {
                $inventoryData = $this->getInventory($accessToken, $itemArray['ItemResponse'][0]['sku']);
                $itemDetails[$itemArray['ItemResponse'][0]['sku']] = $itemArray['ItemResponse'][0];
                $itemDetails[$itemArray['ItemResponse'][0]['sku']]['inventory'] = $inventoryData;
                return $itemDetails;
            }
        }
        return array();
    }

    private function getInventoryData($response, $itemDetails, $accessToken)
    {
        if (!empty($response) && is_array(json_decode($response, true))) {
            $itemArray = json_decode($response, true);
            if (isset($itemArray['ItemResponse'][0]) && !empty($itemArray['ItemResponse'][0])) {
                foreach ($itemArray['ItemResponse'] as $itemResponse) {
                    $inventoryData = $this->getInventory($accessToken, $itemResponse['sku']);
                    $itemDetails[$itemResponse['sku']] = $itemResponse;
                    $itemResponse[$itemResponse['sku']]['inventory_details'] = $inventoryData;
                }
            }
        }
        return $itemDetails;
    }

    public function downloadInventoryViaReport($accessToken, $userId)
    {
        $endpoint = "/v3/reports/reportRequests";
        if (!empty($header)) {
            $header = array_merge($header, $this->defaultHeader);
        } else {
            $header = $this->defaultHeader;
        }
        $mainPath = storage_path('app/public');
        $file_path = $mainPath . '/' . $userId . ".zip";
        $fp = fopen($file_path, 'w+');
        $ch = curl_init($this->url . $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $res = curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        print_r($res);
        die;
        if (filesize($file_path) > 0) {
            $zip = new \ZipArchive();
            $res = $zip->open($file_path);
            print_r($res);
            die;
            if ($res === TRUE) {
                $extractedPath = $mainPath . "/" . $userId;
                $zip->extractTo($extractedPath);
                $zip->close();
                unlink($file_path);
                $files = array_values(array_diff(scandir($extractedPath), array('.', '..')));
                if (isset($files[0]) && !empty($files[0])) {
                    $csv_file_path = $extractedPath . "/" . $files[0];
                    $file = fopen($csv_file_path, 'r');
                    $headers = array();
                    $itemDetails = array();
                    $i = 0;
                    while (($line = fgetcsv($file)) !== FALSE) {
                        print_r($line);
                        if ($i == 0)
                            $headers = $line;
                        else
                            array_push($itemDetails, $line);
                        $i++;
                    }
                    $constructedItemDetails = array();
                    $num = 0;
                    foreach ($itemDetails as $itemDetail) {
                        foreach ($itemDetail as $key => $itemAttribute) {
                            $constructedItemDetails[$num][$headers[$key]] = $itemAttribute;
                        }
                        $num++;
                    }
                    unlink($csv_file_path);
                    rmdir($extractedPath);
                    $items = array();
                    foreach ($constructedItemDetails as $constructedItemDetail) {
                        $items[$constructedItemDetail['SKU']] = $constructedItemDetail;
                    }
                    return $items;
                    fclose($file);
                }
            }
        }
        return array();
    }

    public function callApi($url, $header = array(), $method = "GET", $postData = "")
    {
        if (!empty($header)) {
            $header = array_merge($header, $this->defaultHeader);
        } else {
            $header = $this->defaultHeader;
        }
        print_r($header);
        $curl = curl_init();
        $curl_array = array(
            CURLOPT_URL => $this->url . trim($url),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $header,
        );
        if ($method == 'POST' && !empty($postData))
            $curl_array[CURLOPT_POSTFIELDS] = $postData;
        curl_setopt_array($curl, $curl_array);
        $response = curl_exec($curl);
        $st = curl_getinfo($curl);
               print_r($st);
        curl_close($curl);
        return $response;
    }
}
