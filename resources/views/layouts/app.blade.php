<!doctype html>
<html lang="en">
<head>
    <title>{{ config('app.name', 'Laravel') }} | </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/prism-coy.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/wmart.css')}}">
</head>
<body style="font-size: 12px">
<div id="wmart-app">
    <loader v-if="$root.loading"></loader>
    <loading :active='$root.requestLoader' :is-full-page="fullPage" :loader='loader'></loading>
    <navbar></navbar>
    <headbar></headbar>
    @yield('content')
</div>
<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('js/vendor-all.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ripple.js')}}"></script>
<script src="{{asset('js/pcoded.min.js')}}"></script>
<script src="{{asset('js/horizontal-menu.js')}}"></script>
<script>
    (function () {
        if ($('#layout-sidenav').hasClass('sidenav-horizontal') || window.layoutHelpers.isSmallScreen()) {
            return;
        }
        try {
            window.layoutHelpers._getSetting("Rtl")
            window.layoutHelpers.setCollapsed(
                localStorage.getItem('layoutCollapsed') === 'true',
                false
            );
        } catch (e) {
        }

        $('#layout-sidenav').each(function () {
            new SideNav(this, {
                orientation: $(this).hasClass('sidenav-horizontal') ? 'horizontal' : 'vertical'
            });
        });
        $('body').on('click', '.layout-sidenav-toggle', function (e) {
            e.preventDefault();
            window.layoutHelpers.toggleCollapsed();
            if (!window.layoutHelpers.isSmallScreen()) {
                try {
                    localStorage.setItem('layoutCollapsed', String(window.layoutHelpers.isCollapsed()));
                } catch (e) {
                }
            }
        });

        $("#pcoded").pcodedmenu({
            themelayout: 'horizontal',
            MenuTrigger: 'hover',
            SubMenuTrigger: 'hover',
        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");
    })();


</script>
</body>
</html>


