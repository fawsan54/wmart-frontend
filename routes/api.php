<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::post('/get-product-info', [App\Http\Controllers\ItemController::class, 'getExtensionProductInfo']);
Route::post('/get-price-info', [App\Http\Controllers\ItemController::class, 'getExtensionPriceInfo']);

Route::middleware('web')->get('/user/get-user', function (Request $request) {
    return response()->json(Auth::user());
});

Route::post('/settings/save-app-settings', [App\Http\Controllers\UserController::class, 'saveAppSettings']);
Route::get('/settings/get-app-settings/{key?}', [App\Http\Controllers\UserController::class, 'getAppSettings']);
Route::get('/settings/get-formula/{name}/{id}', [App\Http\Controllers\UserController::class, 'getFormulaByKey']);
Route::put('/settings/update-formula', [App\Http\Controllers\UserController::class, 'updateFormula']);
Route::delete('/settings/delete-formula', [App\Http\Controllers\UserController::class, 'deleteFormula']);
Route::put('/settings/set-as-default-formula', [App\Http\Controllers\UserController::class, 'setasDefaultFormula']);

Route::post('/items/add-item', [App\Http\Controllers\ItemController::class, 'addItem']);
Route::post('/items/get-variations', [App\Http\Controllers\ItemController::class, 'getVariations']);
Route::get('/items/get-items/{asin}', [App\Http\Controllers\ItemController::class, 'getItem']);
Route::get('/items/get-all-items', [App\Http\Controllers\ItemController::class, 'getAllItems']);
Route::get('/items/get-pending-items', [App\Http\Controllers\ItemController::class, 'getPendingItems']);
Route::get('/items/get-item/{id}', [App\Http\Controllers\ItemController::class, 'getItemDetailsById']);
Route::delete('/items/delete-item', [App\Http\Controllers\ItemController::class, 'deleteItem']);
Route::POST('/items/get-selling-price', [App\Http\Controllers\ItemController::class, 'getSellingPrice']);
