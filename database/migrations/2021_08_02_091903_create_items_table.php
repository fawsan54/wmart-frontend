<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id()->index();
            $table->bigInteger('user_id')->index();
            $table->longText('ref_id')->nullable();
            $table->string('walmart_sku')->index();
            $table->longText('title');
            $table->decimal('selling_price')->index()->default(0);
            $table->integer('walmart_stock')->index()->default(0);
            $table->integer('default_qty')->index()->default(0);
            $table->string('supplier_name')->index();
            $table->longText('supplier_item_id');
            $table->boolean('is_variations')->index();
            $table->longText('supplier_variations')->nullable();
            $table->string('seller_name')->index()->nullable();
            $table->decimal('supplier_price')->index()->default(0);
            $table->decimal('supplier_shipping_cost')->index()->default(0);
            $table->integer('supplier_stock')->index()->default(0);
            $table->longText('supplier_item_details')->nullable();
            $table->longText('my_item_details');
            $table->string('buy_box')->index();
            $table->decimal('profit')->index()->default(0);
            $table->tinyInteger('item_status')->index()->default(0);
            $table->tinyInteger('pause_repricing')->index()->default(0);
            $table->integer('handling_time')->index();
            $table->longText('formula_settings');
            $table->bigInteger('batch_id')->index()->nullable();
            $table->string('prepared_at')->index()->nullable();
            $table->tinyInteger('is_updated')->index()->default(0);
            $table->tinyInteger('is_prepared')->index()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
