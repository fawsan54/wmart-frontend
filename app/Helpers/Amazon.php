<?php


namespace App\Helpers;

use MCS\MWSClient;


class Amazon
{
    public function __construct($storeDetails)
    {
        $this->client = new MWSClient($storeDetails);
    }

    public function getProductDetailsById($asin, $idType = 'ASIN', $responseType = 'found')
    {
        $result = $this->client->GetMatchingProductForId($asin, $idType);
        if ($responseType == 'found') {
            if (isset($result['found']) && !empty($result['found'])) {
                return $result['found'];
            }
        } else if ($responseType == 'full') {
            return $result;
        }
        return array();
    }

    public function getLowestOfferListingsForAsin($asin)
    {
        $result = $this->client->GetLowestOfferListingsForASIN($asin, 'new');
        return $result;
    }

    public function getCompetitivePricingForAsin($asin)
    {
        $result = $this->client->GetCompetitivePricingForASIN($asin);
        return $result;
    }

    public function getLowestOfferForFulfilledByMerchant($offerResponse)
    {
        $price = 0;
        $shipping_price = 0;
        if (isset($offerResponse[0])) {
            $i = 0;
            foreach ($offerResponse as $offer) {
                if (isset($offer['Qualifiers']['ItemCondition']) && $offer['Qualifiers']['ItemCondition'] == 'New' &&
                    isset($offer['Qualifiers']['FulfillmentChannel']) && $offer['Qualifiers']['FulfillmentChannel'] == 'Merchant' &&
                    isset($offer['Price']['ListingPrice']['Amount']) && !empty($offer['Price']['ListingPrice']['Amount'])) {
                    if ($price == '0' || ($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']) < $price) {
                        $price = number_format(($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']), 2);
                        $shipping_price = $offer['Price']['Shipping']['Amount'];
                    }
                }
                $i++;
            }
        } else {
            if (isset($offerResponse['Qualifiers']['ItemCondition']) && $offerResponse['Qualifiers']['ItemCondition'] == 'New' &&
                isset($offerResponse['Qualifiers']['FulfillmentChannel']) && $offerResponse['Qualifiers']['FulfillmentChannel'] == 'Merchant' &&
                isset($offerResponse['Price']['ListingPrice']['Amount']) && !empty($offerResponse['Price']['ListingPrice']['Amount'])) {
                $price = number_format(($offerResponse['Price']['ListingPrice']['Amount'] + $offerResponse['Price']['Shipping']['Amount']), 2);
                $shipping_price = $offerResponse['Price']['Shipping']['Amount'];
            }
        }
        return ['listing_price' => $price, 'shipping_price' => $shipping_price];
    }

    public function getLowestOfferForFulfilledByAmazon($offerResponse)
    {
        $price = 0;
        $shipping_price = 0;
        if (isset($offerResponse[0])) {
            $i = 0;
            foreach ($offerResponse as $offer) {
                if (isset($offer['Qualifiers']['ItemCondition']) && $offer['Qualifiers']['ItemCondition'] == 'New' &&
                    isset($offer['Qualifiers']['FulfillmentChannel']) && $offer['Qualifiers']['FulfillmentChannel'] == 'Amazon' &&
                    isset($offer['Price']['ListingPrice']['Amount']) && !empty($offer['Price']['ListingPrice']['Amount'])) {
                    if ($price == '0' || ($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']) < $price) {
                        $price = number_format(($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']), 2);
                        $shipping_price = $offer['Price']['Shipping']['Amount'];
                    }
                }
                $i++;
            }
        } else {
            if (isset($offerResponse['Qualifiers']['ItemCondition']) && $offerResponse['Qualifiers']['ItemCondition'] == 'New' &&
                isset($offerResponse['Qualifiers']['FulfillmentChannel']) && $offerResponse['Qualifiers']['FulfillmentChannel'] == 'Amazon' &&
                isset($offerResponse['Price']['ListingPrice']['Amount']) && !empty($offerResponse['Price']['ListingPrice']['Amount'])) {
                $price = number_format(($offerResponse['Price']['ListingPrice']['Amount'] + $offerResponse['Price']['Shipping']['Amount']), 2);
                $shipping_price = $offerResponse['Price']['Shipping']['Amount'];
            }
        }
        return ['listing_price' => $price, 'shipping_price' => $shipping_price];
    }

    public function getLowestOfferForBuyBox($offerResponse)
    {
        $price = 0;
        $shipping_price = 0;
        $i = 0;
        if (isset($offerResponse[0])) {
            foreach ($offerResponse as $offer) {
                if (isset($offer['@attributes']['condition']) && $offer['@attributes']['condition'] == 'New' &&
                    isset($offer['Price']['ListingPrice']['Amount']) && !empty($offer['Price']['ListingPrice']['Amount'])) {
                    $price = number_format(($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']), 2);
                    $shipping_price = $offer['Price']['Shipping']['Amount'];
                    break;
                }
                $i++;
            }
        } else {
            if (isset($offerResponse['@attributes']['condition']) && $offerResponse['@attributes']['condition'] == 'New' &&
                isset($offerResponse['Price']['ListingPrice']['Amount']) && !empty($offerResponse['Price']['ListingPrice']['Amount'])) {
                $price = number_format(($offerResponse['Price']['ListingPrice']['Amount'] + $offerResponse['Price']['Shipping']['Amount']), 2);
                $shipping_price = $offerResponse['Price']['Shipping']['Amount'];
            }
        }
        return ['listing_price' => $price, 'shipping_price' => $shipping_price];
    }

    public function getLowestOfferForAll($offerResponse)
    {
        $price = 0;
        $shipping_price = 0;
        if (isset($offerResponse[0])) {
            $i = 0;
            foreach ($offerResponse as $offer) {
                if (isset($offer['Qualifiers']['ItemCondition']) && $offer['Qualifiers']['ItemCondition'] == 'New' &&
                    isset($offer['Qualifiers']['FulfillmentChannel']) && isset($offer['Price']['ListingPrice']['Amount']) &&
                    !empty($offer['Price']['ListingPrice']['Amount'])) {
                    if ($price == '0' || ($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']) < $price) {
                        $price = number_format(($offer['Price']['ListingPrice']['Amount'] + $offer['Price']['Shipping']['Amount']), 2);
                        $shipping_price = $offer['Price']['Shipping']['Amount'];
                    }
                }
                $i++;
            }
        } else {
            if (isset($offerResponse['Qualifiers']['ItemCondition']) && $offerResponse['Qualifiers']['ItemCondition'] == 'New' &&
                isset($offerResponse['Qualifiers']['FulfillmentChannel']) && isset($offerResponse['Price']['ListingPrice']['Amount'])
                && !empty($offerResponse['Price']['ListingPrice']['Amount'])) {
                $price = number_format(($offerResponse['Price']['ListingPrice']['Amount'] + $offerResponse['Price']['Shipping']['Amount']), 2);
                $shipping_price = $offerResponse['Price']['Shipping']['Amount'];
            }
        }
        return ['listing_price' => $price, 'shipping_price' => $shipping_price];
    }
}
