@extends('layouts.auth')
@section('content')
    <div class="card">
        <div class="row align-items-center text-center">
            <div class="col-md-12">
                <div class="card-body">
                    <img src="{{asset('images/logo-dark.png')}}" alt="" class="img-fluid mb-4">
                    <h4 class="mb-3 f-w-400">Register</h4>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group mb-3 fill">
                            <label class="floating-label" for="name">Name</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-3 fill">
                            <label class="floating-label" for="email">Email</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Password</label>
                            <input name="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password"
                                   type="password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Confirm Password</label>
                            <input name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror"
                                   id="password_confirmation"
                                   type="password" required>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-block btn-dark has-ripple mb-4">Register</button>
                        <p class="mb-0 text-muted">Already have an account? <a href="{{route('login')}}"
                                                                             class="f-w-400">Login</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

