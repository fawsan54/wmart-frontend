@extends('layouts.auth')
@section('content')
    <div class="card">
        <div class="row align-items-center text-center">
            <div class="col-md-12">
                <div class="card-body">
                    <img src="{{asset('images/logo-dark.png')}}" alt="" class="img-fluid mb-4">
                    <h4 class="mb-3 f-w-400">Reset Password</h4>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="form-group mb-3 fill">
                            <label class="floating-label" for="email">Email address</label>
                            <input type="email" name="email" value="{{ old('email') }}"
                                   class="form-control @error('email') is-invalid @enderror" required autofocus
                                   id="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-block btn-dark has-ripple mb-4">
                            Send Password Reset Link
                        </button>
                        <p class="mb-0 text-muted">Already have an account ? <a href="{{ route('login') }}"
                                                                            class="f-w-400">Login</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

