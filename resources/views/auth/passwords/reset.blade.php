@extends('layouts.auth')
@section('content')
    <div class="card">
        <div class="row align-items-center text-center">
            <div class="col-md-12">
                <div class="card-body">
                    <img src="{{asset('images/logo-dark.png')}}" alt="" class="img-fluid mb-4">
                    <h4 class="mb-3 f-w-400">Reset Password</h4>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group mb-3 fill">
                            <label class="floating-label" for="email">Email address</label>
                            <input type="email" name="email" value="{{ $email ?? old('email') }}"
                                   class="form-control @error('email') is-invalid @enderror" required autofocus
                                   id="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Password</label>
                            <input name="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password"
                                   type="password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4 fill">
                            <label class="floating-label" for="password">Confirm Password</label>
                            <input name="password_confirmation"
                                   class="form-control @error('password_confirmation') is-invalid @enderror"
                                   id="password_confirmation"
                                   type="password" required>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-block btn-dark has-ripple mb-4">
                            Reset Password
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


