<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    const reprice_status = [
        'running' => 0,
        'stopped' => 2,
        'processing' => 1,
    ];
    const item_status = [
        'pending' => 0,
        'active' => 1,
        'expired' => 2,
    ];

    public static function getAllItemsCollections($searchValue, $sortBy, $orderBy, $length, $userInfo)
    {
        $query = Item::where('user_id', $userInfo->id)->where('item_status', self::item_status['active']);
        if (!empty($searchValue)) {
            $query->Where('ref_id', 'Like', '%' . $searchValue . '%');
            $query->orWhere('walmart_sku', 'Like', '%' . $searchValue . '%');
            $query->orWhere('title', 'Like', '%' . $searchValue . '%');
            $query->orWhere('selling_price', 'Like', '%' . $searchValue . '%');
            $query->orWhere('walmart_stock', 'Like', '%' . $searchValue . '%');
            $query->orWhere('supplier_name', 'Like', '%' . $searchValue . '%');
            $query->orWhere('supplier_item_id', 'Like', '%' . $searchValue . '%');
            $query->orWhere('supplier_variations', 'Like', '%' . $searchValue . '%');
            $query->orWhere('seller_name', 'Like', '%' . $searchValue . '%');
            $query->orWhere('supplier_price', 'Like', '%' . $searchValue . '%');
            $query->orWhere('supplier_shipping_cost', 'Like', '%' . $searchValue . '%');
            $query->orWhere('supplier_stock', 'Like', '%' . $searchValue . '%');
            $query->orWhere('profit', 'Like', '%' . $searchValue . '%');
            $query->orWhere('created_at', 'Like', '%' . $searchValue . '%');
            $query->orWhere('updated_at', 'Like', '%' . $searchValue . '%');
        }
        if (!empty($sortBy) && !empty($orderBy))
            $query->orderBy($sortBy, $orderBy);
        $data = $query->paginate($length);
        return $data;
    }

    public static function manageItem($userInfo, $request, $itemDetails, $formula)
    {
        if ($request->id > 0)
            $item = Item::find($request->id);
        else {
            $item = new Item();
            $item->item_status = self::item_status['pending'];
        }
        $item->user_id = $userInfo->id;
        $item->ref_id = $request->ref_id;
        $item->walmart_sku = $request->item_sku;
        $item->title = $itemDetails['productName'];
        if ($item->selling_price == '0.00') {
            $item->walmart_stock = 0;
        } else {
            $item->walmart_stock = $request->default_qty;
        }
        $item->default_qty = $request->default_qty;
        $item->supplier_name = $request->supplier;
        $item->supplier_item_id = $request->supplier_link;
        $item->is_variations = $request->is_variation;
        if ($request->has('supplier_variation') && $request->is_variation == '1')
            $item->supplier_variations = $request->supplier_variation;
        else
            $item->supplier_variations = '';
        $item->my_item_details = json_encode($itemDetails);
        $item->pause_repricing = $request->pause;
        $item->handling_time = $request->handling_time;
        $item->formula_settings = json_encode($formula);
        $item->buy_box = $request->buy_box;
        $item->save();
    }

    public static function getImageUrl($itemDetails)
    {
        if (!empty($itemDetails) && is_array(json_decode($itemDetails, true))) {
            $itemDetailArray = json_decode($itemDetails, true);
            if (isset($itemDetailArray['small_image']) && !empty($itemDetailArray['small_image'])) {
                return '<img src="' . $itemDetailArray['small_image'] . '" style="width:50px;height:50px"/>';
            }
        }
        return '<img src="' . url('/images/no-image.webp') . '" style="width:50px;height:50px"/>';
    }

    public static function getRepriceDetails($created_at, $updated_at)
    {
        $html = "Added On:<br>";
        $html .= date('d-m-Y H:i:s', strtotime($created_at)) . "<br>";
        $html .= "<br>";
        $html .= "Last call:<br>";
        $html .= date('d-m-Y H:i:s', strtotime($updated_at));
        return $html;
    }

    public static function is_valid_url($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL))
            return true;
        return false;
    }

    public static function getSellingPrice($vendor_tax, $margin, $walmart_fee, $other_fee, $supplier_price, $additional_amount = 0)
    {
        $wmartTotlaPrice = ($supplier_price + (($supplier_price * $vendor_tax) / 100));
        $wmartTotlaPrice = ($wmartTotlaPrice + $other_fee) * (($walmart_fee + 100) / 100);
        $wmartTotlaPrice = ($wmartTotlaPrice + $additional_amount) * (($margin + 100) / 100);
        return $wmartTotlaPrice;
    }

    public static function getProfit($wmartTotlaPrice, $vendor_tax, $walmart_fee, $other_fee, $supplier_price)
    {
        return ($wmartTotlaPrice * ((100 - $walmart_fee) / 100)) - $other_fee - (($supplier_price) * ((100 + $vendor_tax) / 100));
    }

    public static function getPrice($vendor_tax, $margin, $walmart_fee, $other_fee, $supplier_price, $minimum_profit)
    {
        $wmartTotlaPrice = self::getSellingPrice($vendor_tax, $margin, $walmart_fee, $other_fee, $supplier_price);
        $profit = self::getProfit($wmartTotlaPrice, $vendor_tax, $walmart_fee, $other_fee, $supplier_price);
        if (empty($minimum_profit) || !is_numeric($minimum_profit))
            $minimum_profit = 0;
        if ($profit < $minimum_profit) {
            $lessingAmount = $minimum_profit - number_format((float)$profit, 2, '.', '');
            $wmartTotlaPrice = self::getSellingPrice($vendor_tax, $margin, $walmart_fee, $other_fee, $supplier_price, $lessingAmount);
            $profit = self::getProfit($wmartTotlaPrice, $vendor_tax, $walmart_fee, $other_fee, $supplier_price);
        }
        $frm_name = "Estimated Fix ";
        $return_txt = "Supplier Price : " . number_format((float)$supplier_price, 2, '.', '') . "$\n";
        $return_txt .= $frm_name . "Price : " . number_format((float)$wmartTotlaPrice, 2, '.', '') . "$\n";
        $return_txt .= $frm_name . "Profit : " . number_format((float)$profit, 2, '.', '') . "$\n";
        $return_txt .= "Minimum Profit  : " . $minimum_profit . "$\n";
        $return_txt .= "Vendor Tax : " . number_format((float)$vendor_tax, 2, '.', '') . "%\n";
        $return_txt .= "Margin : " . number_format((float)$margin, 2, '.', '') . "%\n";
        $return_txt .= "Walmart Fees : " . number_format((float)$walmart_fee, 2, '.', '') . "%";
        return $return_txt;
    }

    public static function getWalmartUrl($walmart_sku, $walmartItemDetails)
    {
        $url = "";
        if (!empty($walmartItemDetails) && is_array(json_decode($walmartItemDetails, true))) {
            $walmartDetailsArray = json_decode($walmartItemDetails, true);
            if (isset($walmartDetailsArray['wpid']) && !empty($walmartDetailsArray['wpid'])) {
                $url = "https://www.walmart.com/ip/" . $walmartDetailsArray['wpid'];
            }
        }
        return '<a target="_blank" href="' . $url . '">' . $walmart_sku . '</a>';
    }

    public static function getStock($stock)
    {
        if ($stock > 0) {
            return '<label class="badge badge-success">in stock</label>';
        }
        return '<label class="badge badge-danger">out of stock</label>';
    }

    public static function getSupplierUrl($supplier_item_id, $supplier)
    {
        $supplier = ucfirst($supplier);
        $url = "";
        if (self::is_valid_url($supplier_item_id))
            $url = $supplier_item_id;
        if (empty($url)) {
            $model = 'App\Models\\' . $supplier;
            if (class_exists($model)) {
                $url = $model::getSupplierUrl($supplier_item_id);
            }
        }
        return '<a href="' . $url . '" target="_blank">' . $supplier_item_id . '</a>';
    }

    public static function getPendingItems($userId)
    {
        return Item::where('item_status', self::item_status['pending'])->where('user_id', $userId)->get();
    }

    public static function getItem($id, $user_id)
    {
        return \App\Models\Item::where('user_id', $user_id)->where('id', $id)->get();
    }

    public static function deleteItem($user_id, $id)
    {
        return Item::where('user_id', $user_id)->where('id', $id)->delete();
    }

    public static function getItemStatus($reprice_status, $is_prepared)
    {
        $html = "";
        if ($reprice_status == self::reprice_status['running']) {
            $html .= '<label class="badge badge-success"><small>Running</small></label>';
        } else {
            $html .= '<label class="badge badge-danger"><small>Stopped</small></label>';
        }
        if ($is_prepared == self::reprice_status['processing']) {
            $html .= '<br> <label class="badge badge-info">Processing</label>';
        }
        return $html;
    }

    public static function getSupplierVariations($supplier_variation, $supplierItemDetails)
    {
        $variation = "N/A";
        if (!empty($supplierItemDetails) && !empty($supplier_variation) && is_array(json_decode($supplierItemDetails, true))) {
            $supplierArray = json_decode($supplierItemDetails, true);
            if (isset($supplierArray['Relationships']) && !empty($supplierArray['Relationships'])) {
                foreach ($supplierArray['Relationships'] as $variations) {
                    if (isset($variations['Identifiers']['MarketplaceASIN']['ASIN']) && $variations['Identifiers']['MarketplaceASIN']['ASIN'] == $supplier_variation) {
                        $variationArray = $variations;
                        unset($variationArray['Identifiers']);
                        $variationDatas = array();
                        foreach ($variationArray as $name => $value)
                            array_push($variationDatas, $name . " : " . $value);
                        $variation = implode(' | ', $variationDatas);
                        break;
                    }
                }
            }
        }
        return $variation;
    }

    public static function getExtensionBuyLists($user_id)
    {
        $item = new Item;
        $item->setConnection(env('DB_EXTENSION_CONNECTION_NAME'));
        $item->setTable('buy_lists');
        return $item->where('user_id', $user_id)->get();
    }
}
