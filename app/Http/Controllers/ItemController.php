<?php

namespace App\Http\Controllers;

use App\Helpers\Walmart;
use App\Models\Item;
use Illuminate\Http\Request;
use \App\Helpers\Amazon;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;

class ItemController extends Controller
{
    public function __construct(Request $req)
    {
        if (!empty($req->header('Authorization'))) {
            if (empty($req->header('is-extension')))
                $this->middleware('auth:api');
            $this->is_api = 1;
        } else {
            $this->middleware('web');
            $this->is_api = 0;
        }
    }

    public function getExtensionPriceInfo(Request $request)
    {
        $this->validate($request, [
            'asin' => 'required|array'
        ]);
        //        header('Access-Control-Allow-Origin: *');
        $userInfo = json_decode(base64_decode(str_replace('Bearer ', '', $request->header('Authorization'))));
        if (isset($userInfo->id) && !empty($userInfo->id)) {
            $userInfo = \App\Models\User::getExtensionUser($userInfo->id);
            $storeSettings = array();
            if (isset($userInfo->settings) && !empty($userInfo->settings)) {
                if (isset($userInfo->settings) && !empty($userInfo->settings)) {
                    $marketplaceSettings = \App\Models\User::getSetting($userInfo->settings, 'marketplace_settings');
                    if (isset($marketplaceSettings['amazon']) && !empty($marketplaceSettings['amazon'])) {
                        $storeSettings = \App\Models\User::constructAmazonStoreArray($marketplaceSettings['amazon']);
                    }
                }
            }
            if (empty($storeSettings))
                $storeSettings = \App\Models\User::constructDefaultAmazonStoreArray();
            $amazon = new Amazon($storeSettings);
            return response()->json(['datas' => $this->getExtensionOffer($request->asin, $amazon)])
                ->withHeaders(['Access-Control-Expose-Headers' => 'Authorization', 'Authorization' => 'Bearer ' . base64_encode(json_encode($userInfo->toArray()))]);
        }
        return response()->json(['error' => 'invalid request']);
    }

    private function getExtensionOffer($asin_array, $amazon)
    {
        $api_response = array();
        //        header('Access-Control-Allow-Origin: *');
        $asin_chunk_array = array_chunk($asin_array, 10, true);
        //        if (empty($source) ||
        //            (in_array('Amazon', $source) && in_array('Amazon FBA', $source)) ||
        //            in_array('Amazon FBA', $source)) {
        //
        //        } else if (in_array('Amazon', $source)) {
        //            foreach ($asin_chunk_array as $asins) {
        //                $asins = array_keys($asins);
        //                $buyBox = $amazon->getCompetitivePricingForAsin($asins);
        //                foreach ($asins as $asin) {
        //                    if (isset($buyBox[$asin])) {
        //                        $lowestOffer = $amazon->getLowestOfferForBuyBox($buyBox[$asin]);
        ////                        $api_response[$asin]['buybox_price_info'] = $lowestOffer;
        ////                        $api_response[$asin]['lowest_price_info'] = $lowestOffer;
        //                        $api_response[$asin]['lowest_price_info']['Amazon'] = $amazon->getLowestOfferForFulfilledByAmazon($lowestOffers[$asin]);
        //                        $api_response[$asin]['lowest_price_info']['Merchent'] = $amazon->getLowestOfferForFulfilledByMerchant($lowestOffers[$asin]);
        //                    }
        //                }
        //            }
        //        }
        foreach ($asin_chunk_array as $asins) {
            $lowestOffers = $amazon->getLowestOfferListingsForAsin($asins);
            //            $buyBox = $amazon->getCompetitivePricingForAsin($asins);
            foreach ($asins as $asin) {
                if (isset($lowestOffers[$asin])) {
                    $api_response[$asin]['lowest_price_info']['Amazon'] = $amazon->getLowestOfferForFulfilledByAmazon($lowestOffers[$asin]);
                    $api_response[$asin]['lowest_price_info']['Merchent'] = $amazon->getLowestOfferForFulfilledByMerchant($lowestOffers[$asin]);
                    //                    $api_response[$asin]['buybox_price_info'] = $amazon->getLowestOfferForBuyBox($buyBox[$asin]);
                }
                //                else if (isset($lowestOffers[$asin]) && !isset($buyBox[$asin])) {
                //                    $api_response[$asin]['lowest_price_info']['Amazon'] = $amazon->getLowestOfferForFulfilledByAmazon($lowestOffers[$asin]);
                //                    $api_response[$asin]['lowest_price_info']['Merchent'] = $amazon->getLowestOfferForFulfilledByMerchant($lowestOffers[$asin]);
                //                    if (count($source) === 0 || count($source) === 2 || in_array('Amazon', $source)) {
                //                        $api_response[$asin]['buybox_price_info'] = $amazon->getLowestOfferForAll($lowestOffers[$asin]);
                //                    } else if (in_array('Amazon FBA', $source)) {
                //                        $api_response[$asin]['buybox_price_info'] = $amazon->getLowestOfferForFulfilledByAmazon($lowestOffers[$asin]);
                //                    }
                //                }
                else {
                    $api_response[$asin]['lowest_price_info'] = array();
                    //                    $api_response[$asin]['buybox_price_info'] = array();
                    //                    $api_response[$asin]['supplier_price'] = $asin_array[$asin];
                }
                //                if ($asin_array[$asin] == '0') {
                //                    $api_response[$asin]['supplier_price'] = 0;//$api_response[$asin]['lowest_price_info']['listing_price']
                //                } else {
                //                    $api_response[$asin]['supplier_price'] = $asin_array[$asin];
                //                }
            }
        }
        return $api_response;
    }

    public function getExtensionProductInfo(Request $request)
    {
        //        header('Access-Control-Allow-Origin: *');
        $this->validate($request, [
            'upc' => 'required|array',
            'requestId' => 'required'
        ]);
        $userInfo = json_decode(base64_decode(str_replace('Bearer ', '', $request->header('Authorization'))));
        if (isset($userInfo->id) && !empty($userInfo->id)) {
            $userInfo = \App\Models\User::getExtensionUser($userInfo->id);
            if (isset($userInfo->settings) && !empty($userInfo->settings)) {
                $marketplaceSettings = \App\Models\User::getSetting($userInfo->settings, 'marketplace_settings');
                if (isset($marketplaceSettings['amazon']) && !empty($marketplaceSettings['amazon'])) {
                    $storeSettings = \App\Models\User::constructAmazonStoreArray($marketplaceSettings['amazon']);
                }
            }
            if (empty($storeSettings))
                $storeSettings = \App\Models\User::constructDefaultAmazonStoreArray();
            $amazon = new Amazon($storeSettings);
            $upcListArray = array_chunk($request->upc, 5);
            $responseArray = array();
            foreach ($upcListArray as $upc) {
                $response = $amazon->getProductDetailsById($upc, 'UPC', 'full');
                if (!isset($response['found'])) {
                    sleep(rand(1, 5));
                    $response = $amazon->getProductDetailsById($upc, 'UPC', 'full');
                }
                foreach ($upc as $id) {
                    if (isset($response['found'][$id][0]['ASIN']) && !empty($response['found'][$id][0]['ASIN'])) {
                        if (count($response['found'][$id]) > 1) {
                            foreach ($response['found'][$id] as $productResponse) {
                                $priceOffers = $amazon->getLowestOfferListingsForAsin([$productResponse['ASIN']]);
                                if (!empty($priceOffers[$productResponse['ASIN']])) {
                                    $responseArray[$id] = $productResponse;
                                    break;
                                }
                            }
                            if (!isset($responseArray[$id]) || empty($responseArray[$id]))
                                $responseArray[$id] = $response['found'][$id][0];
                        } else {
                            $responseArray[$id] = $response['found'][$id][0];
                        }
                    } else {
                        $responseArray[$id] = array();
                    }
                }
            }
            $buy_lists_records = \App\Models\Item::getExtensionBuyLists($userInfo->id);
            $buy_lists = array();
            foreach ($buy_lists_records as $buy_lists_record) {
                if (isset($buy_lists_record->row_details) && !empty($buy_lists_record->row_details)) {
                    $row_array = json_decode($buy_lists_record->row_details, true);
                    if (isset($row_array['upc']) && !empty($row_array['upc'])) {
                        $row_array['from_db'] = true;
                        array_push($buy_lists, $row_array);
                    }
                }
            }
            return response()->json([
                'datas' => $responseArray,
                'buy_lists' => $buy_lists,
                'requestId' => $request->requestId
            ])->withHeaders(['Access-Control-Expose-Headers' => 'Authorization', 'Authorization' => 'Bearer ' . base64_encode(json_encode($userInfo->toArray()))]);
        }
    }

    public function deleteItem(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        \App\Models\Item::deleteItem($userInfo->id, $request->id);
        return response()->json(['error' => 0, 'message' => 'Item successfully deleted']);
    }

    public function getPendingItems()
    {
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        $userItems = \App\Models\Item::getPendingItems($userInfo->id);
        return response()->json(['error' => 0, 'data' => $userItems]);
    }

    public function getItemDetailsById($id)
    {
        if (!is_numeric($id))
            return response()->json(['error' => 1, 'message' => 'invalid request']);
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        $itemDetails = \App\Models\Item::getItem($id, $userInfo->id);
        if (isset($itemDetails[0]->id)) {
            return response()->json(['error' => 0, 'data' => $itemDetails[0]]);
        }
        return response()->json(['error' => 1, 'message' => 'invalid item']);
    }

    public function getAllItems(Request $request)
    {
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        //        $settings = \App\Models\User::getSetting($userInfo->settings);
        //        $walmart = new Walmart($settings['marketplace_settings']['walmart']);
        //        $accessToken = $walmart->authenticate();
        //        echo $accessToken;
        //        die;
        $itemsCollection = \App\Models\Item::getAllItemsCollections($request->input('search'), $request->input('column'), $request->input('dir'), $request->input('length'), $userInfo);
        foreach ($itemsCollection as $k => $da) {
            $itemsCollection[$k]->image_url = \App\Models\Item::getImageUrl($da->supplier_item_details);
            $itemsCollection[$k]->repricer_details = \App\Models\Item::getRepriceDetails($da->created_at, $da->updated_at);
            $itemsCollection[$k]->supplier_item_id = \App\Models\Item::getSupplierUrl($da->supplier_item_id, $da->supplier_name);
            $itemsCollection[$k]->item_status = \App\Models\Item::getItemStatus($da->pause_repricing, $da->is_prepared);
            $itemsCollection[$k]->supplier_name = ucfirst($da->supplier_name);
            $itemsCollection[$k]->supplier_variations = \App\Models\Item::getSupplierVariations($da->supplier_variations, $da->supplier_item_details);
            $itemsCollection[$k]->walmart_stock = \App\Models\Item::getStock($da->walmart_stock);
            $itemsCollection[$k]->supplier_stock = \App\Models\Item::getStock($da->supplier_stock);
            $itemsCollection[$k]->walmart_sku = \App\Models\Item::getWalmartUrl($da->walmart_sku, $da->my_item_details);
            $itemsCollection[$k]->title = \App\Models\Item::getWalmartUrl($da->title, $da->my_item_details);
        }
        return new DataTableCollectionResource($itemsCollection);
    }

    public function addItem(Request $request)
    {
        $this->validate($request, [
            'supplier_link' => 'required',
            'is_variation' => 'required',
            'supplier' => 'required',
            'handling_time' => 'required',
            'pause' => 'required',
            'default_qty' => 'required',
            'buy_box' => 'required',
        ]);
        if (!empty($request->id)) {
            $this->validate($request, [
                'item_sku' => 'required|unique:items,walmart_sku,' . $request->id,
            ]);
        } else {
            $this->validate($request, [
                'item_sku' => 'required|unique:items,walmart_sku',
            ]);
        }
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        $settings = \App\Models\User::getSetting($userInfo->settings);
        $formula = array();
        if ($request->formula_name == '0') {
            $this->validate($request, [
                'profit_formula.add_to_formula' => 'required',
                'profit_formula.vendor_tax' => 'required',
                'profit_formula.selling_fees' => 'required',
            ]);
            if ($request->profit_formula['add_to_formula'] == '1') {
                $this->validate($request, [
                    'profit_formula.formula_name' => 'required'
                ]);
                if (!isset($settings['formula_settings']['profit_formula']))
                    $settings['formula_settings']['profit_formula'] = array();
                $profit_formula = [
                    'formula_name' => $request->profit_formula['formula_name'],
                    'vendor_tax' => $request->profit_formula['vendor_tax'],
                    'selling_fees' => $request->profit_formula['selling_fees']
                ];
                array_push($settings['formula_settings']['profit_formula'], $profit_formula);
                \App\Models\User::saveAppSettings($settings, $userInfo->id);
            } else {
                $profit_formula['vendor_tax'] = $request->profit_formula['vendor_tax'];
                $profit_formula['selling_fees'] = $request->profit_formula['selling_fees'];
            }
        } else {
            if (isset($settings['formula_settings']['profit_formula'][($request->formula_name - 1)]) && !empty($settings['formula_settings']['profit_formula'][($request->formula_name - 1)]))
                $profit_formula = $settings['formula_settings']['profit_formula'][($request->formula_name - 1)];
            else
                return response()->json(['error' => 1, 'message' => 'Invalid profit formula']);
        }
        if ($request->selling_formula_name == '0') {
            $this->validate($request, [
                'selling_price_formula.add_to_formula' => 'required',
                'selling_price_formula.supplier_tax' => 'required',
                'selling_price_formula.walmart_fee' => 'required',
                'selling_price_formula.margin' => 'required',
                'selling_price_formula.selling_fees' => 'required',
            ]);
            if ($request->has('minimum_profit') && !empty($request->minimum_profit)) {
                $this->validate($request, [
                    'minimum_profit' => 'required|numeric',
                ]);
            }
            if ($request->selling_price_formula['add_to_formula'] == '1') {
                $this->validate($request, [
                    'selling_price_formula.formula_name' => 'required'
                ]);
                if (!isset($settings['formula_settings']['selling_price_formula']))
                    $settings['formula_settings']['selling_price_formula'] = array();
                $selling_price_formula = [
                    'formula_name' => $request->selling_price_formula['formula_name'],
                    'supplier_tax' => $request->selling_price_formula['supplier_tax'],
                    'minimum_profit' => $request->selling_price_formula['minimum_profit'],
                    'walmart_fee' => $request->selling_price_formula['walmart_fee'],
                    'margin' => $request->selling_price_formula['margin'],
                    'selling_fees' => $request->selling_price_formula['selling_fees']
                ];
                array_push($settings['formula_settings']['selling_price_formula'], $selling_price_formula);
                \App\Models\User::saveAppSettings($settings, $userInfo->id);
            } else {
                $selling_price_formula['supplier_tax'] = $request->selling_price_formula['supplier_tax'];
                $selling_price_formula['walmart_fee'] = $request->selling_price_formula['walmart_fee'];
                $selling_price_formula['margin'] = $request->selling_price_formula['margin'];
                $selling_price_formula['selling_fees'] = $request->selling_price_formula['selling_fees'];
                $selling_price_formula['minimum_profit'] = $request->selling_price_formula['minimum_profit'];
            }
        } else {
            if (isset($settings['formula_settings']['selling_price_formula'][($request->selling_formula_name - 1)]) && !empty($settings['formula_settings']['selling_price_formula'][($request->selling_formula_name - 1)]))
                $selling_price_formula = $settings['formula_settings']['selling_price_formula'][($request->selling_formula_name - 1)];
            else
                return response()->json(['error' => 1, 'message' => 'Invalid selling price formula']);
        }
        $formula['selling_price_formula'] = $selling_price_formula;
        $formula['profit_formula'] = $profit_formula;
        return $this->processItem($settings, $userInfo, $request, $formula);
    }

    private function processItem($settings, $userInfo, $request, $formula)
    {
        if (isset($settings['marketplace_settings']['walmart']['client_id']) && isset($settings['marketplace_settings']['walmart']['client_secret']) && !empty($settings['marketplace_settings']['walmart']['client_id']) && !empty($settings['marketplace_settings']['walmart']['client_secret'])) {
            $walmart = new Walmart($settings['marketplace_settings']['walmart']);
            $accessToken = $walmart->authenticate();
            if (!$accessToken)
                return response()->json(['error' => 1, 'message' => 'could not authenticate to walmart store']);
            // $myInventory = $walmart->downloadInventory($accessToken, $request->item_sku);
            $myInventory = $walmart->getItemPriceReport($accessToken);
            print_r($myInventory);
            die;
            if (isset($myInventory[$request->item_sku]) && !empty($myInventory[$request->item_sku])) {
                $itemDetails = $myInventory[$request->item_sku];
                \App\Models\Item::manageItem($userInfo, $request, $itemDetails, $formula);
                if ($request->id > 0)
                    return response()->json(['error' => 0, 'message' => 'Item successfully updated']);
                else
                    return response()->json(['error' => 0, 'message' => 'Item successfully added to queue']);
            } else {
                return response()->json(['error' => 1, 'message' => 'Invalid Sku']);
            }
        } else {
            return response()->json(['error' => 1, 'message' => 'walmart store settings not found']);
        }
    }

    public function getVariations(Request $request)
    {
        $this->validate($request, [
            'supplier_link' => 'required|max:10'
        ]);
        $variations = array();
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        if (isset($userInfo->settings) && !empty($userInfo->settings)) {
            $marketplaceSettings = \App\Models\User::getSetting($userInfo->settings, 'marketplace_settings');
            if (isset($marketplaceSettings['amazon']) && !empty($marketplaceSettings['amazon'])) {
                $storeSettings = \App\Models\User::constructAmazonStoreArray($marketplaceSettings['amazon']);
                $amazon = new Amazon($storeSettings);
                $productInfo = $amazon->getProductDetailsById($request->supplier_link);
                $variations = \App\Models\Amazon::getVariations($productInfo);
            }
        }
        return response()->json(['error' => 0, 'variations' => $variations]);
    }

    public function getItem($asin)
    {
        if (isset($userInfo->settings) && !empty($userInfo->settings)) {
            $marketplaceSettings = \App\Models\User::getSetting($userInfo->settings, 'marketplace_settings');
            if (isset($marketplaceSettings['amazon']) && !empty($marketplaceSettings['amazon'])) {
                $storeSettings = \App\Models\User::constructAmazonStoreArray($marketplaceSettings['amazon']);
                $amazon = new Amazon($storeSettings);
                $productInfo = $amazon->getProductDetailsById($asin);
                if (isset($productInfo[$asin])) {
                }
                print_r($productInfo);
                die;
            }
        }
    }

    public function getSellingPrice(Request $request)
    {
        $this->validate($request, [
            'supplierPrice' => 'required|numeric',
            'shippingPrice' => 'required|numeric',
            'formula.supplier_tax' => 'required|numeric',
            'formula.walmart_fee' => 'required|numeric',
            'formula.margin' => 'required|numeric',
            'formula.selling_fees' => 'required|numeric',
        ]);
        if ($request->has('formula.minimum_profit') && !empty($request->formula['minimum_profit'])) {
            $this->validate($request, [
                'formula.minimum_profit' => 'required|numeric',
            ]);
        }
        $price_details = \App\Models\Item::getPrice($request->formula['supplier_tax'], $request->formula['margin'], $request->formula['walmart_fee'], $request->formula['selling_fees'], ($request->supplierPrice + $request->shippingPrice), $request->formula['minimum_profit']);
        return response()->json(['error' => 0, 'message' => $price_details]);
    }
}
