<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct(Request $req)
    {
        if (!empty($req->header('Authorization'))) {
            $this->middleware('auth:api');
            $this->is_api = 1;
        } else {
            if (\Request::route()->getName() != 'extension-login') {
                $this->middleware('web');
                $this->is_api = 0;
            }
        }
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if (Auth::once($credentials)) {
            return response()->json(['data' => \App\Models\User::findUserByEmail($request->email)]);
        }
        return response()->json(['errors' => [
            'The provided credentials do not match our records.',
        ]]);
    }

    public function setasDefaultFormula(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'formula_type' => 'required',
        ]);
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        $Settings = \App\Models\User::getSetting($userInfo->settings);
        if (isset($Settings['formula_settings'][$request->formula_type]) && !empty($Settings['formula_settings'][$request->formula_type])) {
            $formulaSettings = $Settings['formula_settings'][$request->formula_type];
            foreach ($formulaSettings as $key => $formulaSetting) {
                if ($key == $request->id)
                    $formulaSettings[$key]['is_default'] = 1;
                else
                    $formulaSettings[$key]['is_default'] = 0;
            }
            $Settings['formula_settings'][$request->formula_type] = $formulaSettings;
            \App\Models\User::saveAppSettings($Settings, $userInfo->id);
        }
        return response()->json(['error' => 0, 'message' => 'Settings successfully saved']);
    }

    public function deleteFormula(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'formula_type' => 'required',
        ]);
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        $Settings = \App\Models\User::getSetting($userInfo->settings);
        if (isset($Settings['formula_settings'][$request->formula_type]) && !empty($Settings['formula_settings'][$request->formula_type])) {
            $formulaSettings = $Settings['formula_settings'][$request->formula_type];
            unset($formulaSettings[$request->id]);
            $formulaSettingsArray = array_values($formulaSettings);
            $Settings['formula_settings'][$request->formula_type] = $formulaSettingsArray;
            \App\Models\User::saveAppSettings($Settings, $userInfo->id);
        }
        return response()->json(['error' => 0, 'message' => 'Settings successfully saved']);
    }

    public function updateFormula(Request $request)
    {
        $this->validate($request, [
            'formula_name' => 'required',
            'formula_type' => 'required',
            'id' => 'required|numeric',
            'selling_fees' => 'required|numeric',
        ]);
        if ($request->formula_type == 'selling_price_formula') {
            $this->validate($request, [
                'supplier_tax' => 'required|numeric',
                'margin' => 'required|numeric',
                'walmart_fee' => 'required|numeric',
            ]);
            if ($request->has('minimum_profit') && !empty($request->minimum_profit)) {
                $this->validate($request, [
                    'minimum_profit' => 'required|numeric',
                ]);
            }
        } elseif ($request->formula_type == 'profit_formula') {
            $this->validate($request, [
                'vendor_tax' => 'required|numeric',
            ]);
        }
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        \App\Models\User::saveAppSettingsByName($userInfo->settings, 'formula_settings', $request->all(), $userInfo->id);
        return response()->json(['error' => 0, 'message' => 'Successfully updated']);
    }

    public function getFormulaByKey($name, $key)
    {
        if (!is_numeric($key) || empty($name))
            return response()->json(['error' => 1, 'message' => 'Invalid formula identifier']);
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        $formulaSettings = \App\Models\User::getSetting($userInfo->settings, 'formula_settings');
        if (isset($formulaSettings[$name][$key]) && !empty($formulaSettings[$name][$key])) {
            return response()->json(['error' => 0, 'data' => $formulaSettings[$name][$key]]);
        }
        return response()->json(['error' => 1, 'message' => 'Invalid formula']);
    }

    public function saveAppSettings(Request $request)
    {
        $this->validate($request, [
            'settings_mode' => 'required',
        ]);
        if ($request->settings_mode == 'general_settings') {
            $this->validate($request, [
                'handling_time' => 'required|numeric',
                'default_qty' => 'required|numeric',
                'display_per_page' => 'required|numeric',
            ]);
        } elseif ($request->settings_mode == 'marketplace_settings') {
            if ($request->marketplace == 'amazon') {
                $this->validate($request, [
                    'amazon.merchent_id' => 'required',
                    'amazon.martketplace_id' => 'required',
                    'amazon.auth_token' => 'required',
                ]);
            } elseif ($request->marketplace == 'walmart') {
                $this->validate($request, [
                    'walmart.client_id' => 'required',
                    'walmart.client_secret' => 'required',
                ]);
            }
        }
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        \App\Models\User::saveAppSettingsByName($userInfo->settings, $request->settings_mode, $request->all(), $userInfo->id);
        return response()->json(['error' => 0, 'message' => 'Settings successfully saved']);
    }

    public function getAppSettings($key = "")
    {
        $settings = array();
        $userInfo = \App\Models\User::getUserInfo($this->is_api);
        if (isset($userInfo->settings) && !empty($userInfo->settings) && !is_null($userInfo->settings)) {
            $settings = json_decode($userInfo->settings, true);
        }
        if (!empty($key) && isset($settings[$key]))
            return response()->json(['error' => 0, 'data' => $settings[$key]]);
        elseif (!empty($key) && !isset($settings[$key]))
            return response()->json(['error' => 0, 'data' => []]);
        else
            return response()->json(['error' => 0, 'data' => $settings]);
    }
}
